/*
 *  SinOscillator.cpp
 *  sdaAudioMidi
 *
 *  Created by tjmitche on 11/11/2010.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 *      Practical 8
 *
 */

#include "SinOscillator.hpp"
#include <cmath>

SinOscillator::SinOscillator()
{
    reset();
}

SinOscillator::~SinOscillator()
{
    
}

float SinOscillator::renderWaveShape (const float currentPhase)
{
    return sin (currentPhase);
}
