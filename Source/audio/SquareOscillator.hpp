//
//  SquareOscillator.hpp
//  JuceBasicAudio
//
//  Created by Joseph Horton on 13/11/2017.
//
//

#ifndef SquareOscillator_hpp
#define SquareOscillator_hpp

#include <stdio.h>
#include "Oscillator.hpp"

class SquareOscillator : public Oscillator
{
public:
    SquareOscillator();
    ~SquareOscillator();
    
    float renderWaveShape(const float currentPhase) override;
private:
};

#endif /* SquareOscillator_hpp */
