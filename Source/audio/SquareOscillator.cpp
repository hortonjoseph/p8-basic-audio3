//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Joseph Horton on 13/11/2017.
//
//

#include "SquareOscillator.hpp"

SquareOscillator::SquareOscillator()
{
    
}

SquareOscillator::~SquareOscillator()
{
    
}

float SquareOscillator::renderWaveShape(const float currentPhase)
{
    return square(currentPhase);
}
